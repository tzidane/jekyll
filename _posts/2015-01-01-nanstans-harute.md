---
layout: post
title: Nånståns Härute
tags: [writing, Swedish, not-programming]
---
Bara en liten bit av Brooklyn 
När en trend slutar vara trendig och blir till det vanliga

Jag är en amerikan som har bott i Brooklyn i fyra år. Jag har också bott i Stockholm i ett år för ett par år sen. De två städerna kunde inte vara mer olika. 
Jag flyttade till New York när jag var 18, utan att veta ett dugg om stand och utan att känna någon som bodde här. Jag sov i Central Park i två dagar tills jag lyckades få en lägenhet, hittadpå Craigslist i en Starbucks, där jag sov på den gamla soffan bredvid två medelålders missbrukare. Och för bara 6000kr i månaden. 
Eftersom jag inte kände en enda person brukade jag spendera min tid slumpmässigt vandrandes på gatorna i timtal, läsandes, eller tittande på folk på tunnelbanan. Livet var inte så dåligt ändå, för att jag hade tre månader att upptäcka – med nästan en osynlighetsmantel–NYC. Men alla kan vi ibland känna oss ensamma, “ingen man är en ö,” osv. 
Det var på perrongen där jag satt för att kliva på nästa tåg destination vart som helst som jag träffade min första kompis. Han som satt bredvid mig lyssnade på två personer som spelade dragspel som jag hade sett förut. Jag berättade för honom att de var bra; att jag hade sett de spela Lady Gaga. 
Så började vi prata. Han hade bott i Sverige i ett par månader och hade några kompisar därifrån som han presenterade för mig. En av de svenskarna skulle jag gifta mig med knappt ett år senare. 

När jag flyttade till Stockholm var det såklart mycket snack om Brooklyn för att det var där min man och jag hade träffats. Men jag skulle tro att även om jag hade flyttat till Stockholm  från min hemstad i Michigan, så skulle jag ha hört talas om Brooklyn nästan lika mycket. 
Alla var, ja helt enkelt, besatta av Brooklyn. Jag förstod till viss del varför, men samtidigt så hade jag en hel nya city framför mig, som inte liknade någon jag någonsin sett förut: slott, skärgården, arkitekturen… Jag var lika förtrollad som när jag flyttade till Brooklyn. Men alla ville hellre prata om Brooklyn, det skulle det alltid dyka upp en gång förr eller senare. 
Jag började se ett mönster. Alla som hade varit i NYC pratade om samma ställen: restauranger, klubbar, affärer. Jag insåg att de hade gjort vad många gör—hängt i den 3 kilometer radius som består av Williamsburg, Bushwick, Greenpoint, och södra Manhattan. Sådana personer liknar de som “aldrig” lämnar Södermalm.
Under detre månader jag vandrade runt staden så gick jag aldrig på samma gator. Jag gick från min lägenhet till Coney Island (en tre timmars promenad), till Forest Hills, Queens (två timmar bara), tog tunnelbanan till The Bronx och gick sen runt tills det blev midnatt, sen stannade jag uppe hela natten och gickrunt the Upper East Side, och sen Upper West en annan kväll, sen hängde jag i Battery Park, Canarsie Pier, nära LaGuardia flygplatsen (jag vet). 
Kanske var jag lite mer äventyrlig (galen) än de flesta, men ändå jag fattade inte varför bokstavligen alla gjorde samma saker på deras (tremånadersseemester, vanligtvis, någonting ganska oerhört i USA) här. 
Sen gick jag till Bar Brooklyn i Hornstull för första gången. Jag visste inte att det fanns en plats som hette så tills en kompis föreslog att vi skulle gå dit istället för Riche efter att jag varit i Stockholm i ett par veckor. 
Jag minns att jag var lite mer uppmärksam och tittade noggrant på miljön men inte hittade någonting i baren som hade att göra med Brooklyn annat än en Brooklyn Brewery skylt. Jag frågade varför den hette så, men ingen hade något svar. Det såg ut som en bar.

Tre år senare och jag undrade om trenden hade försvunnit. Svaret: ett stort nej. 
Om man gör en snabb internet sökning så hittar man mycket snack om Brooklyn och NYC i Stockholm. Exempelvis: Wallenstams New York 1 i Ladugårdsgärdet. På deras webbsida stog det att “På gärdet, i kvarteret New York, kommer vi att uppföra en av våra mer exklusiva bostadsrättsprojekt.” Exklusiva, jag fattar. En som lämnade en kommentar på annonsen sa: 

“Kan detta löjliga New York-komplex få ett slut någon gång?!”

Det här är inget nytt—i 2013 Grub Street publicerade “Så Brooklyn: How Kings County Became The Coolest Thing in Sweden.” 
I en artikel i DN i 2014 med titeln “Många likheter mellan Hornstull och Brooklyn,” där frågade folk på gatorna, “Har [besatthet med Brooklyn] gått för långt?” så bemötte en tjej frågan, till synes utan ironi: 

“Jag ska flytta till Brooklyn snart så jag får ta reda på det då. Till Williamsburg, den lite mer ruffa delen, under bron. Lite som vid Liljeholmsbron ungefär.”

Gissa vad: det var faktiskt där jag fick min första lägenhet för 6000kr i månaden. För ett par månader sedan i oktober hyrde en kompis en Airbnb i den “lite mer ruffa delen,” under Williamsburgbron för 10,000kr i veckan. Den ligger mellan en utav de mest gentriferade delarna av Willamsburg norrut, och söderåt ett kvarter som är nästan 99% chassidiskt, en grupp ultraortodoxa judar som helt och hållet håller sig för sig själva. Vad menade hon med “Ruff”? Tuff att betala hennes hyra? 
2014, publicerade BBC en video-artikel med titeln, “Why does Stockholm love Brooklyn?”.  Däri, tillkännager Henrik Evrell som jobbar för det besynnerliga 70 Agency följande:

“Jag tycker att svenskar alltid har haft en kärleksaffär med NYC. Vi har alltid varit internationella—vi växte upp så […] Brooklyn representerar skärning av det lantliga med det urbana eller det earthly (jordliga) och det här är något som svenskar gillar jättemycket just nu. 

Earthly, sa han? Brooklyn ligger i hjärtat av det största storstadsområdet i USA, och en av de största i världen—det lantliga är en två timmars bilresa borta.
The Brooklyn Brewerynämndes också i den korta videon. Gissa var den ligger—korrekt: i Williamsburg. Och nu i Stockholm också. Det “Nya Carnegiebryggeriet ligger i anrika Lumahuset, mitt i hjärtat av Hammarby Sjöstad,” som föddes ut av ett samarbete mellan Carlsberg och Brooklyn Brewery. 
Ja—Brooklyn Brewery är i maskopi, men bryggeriet är kort sagt ett turistmål och företaget själv är multinationellt och försöker verka som en liten craft brewery. 
I samma BBC video, så säger Burt McRoy från Bar Brooklyn: 

“There’s a sort of fantasy about New York and Brooklyn through movies, and books and TV […] and I think that Bar Brooklyn sort of encompasses in what we try to do in making the culture from New York, come to Stockholm.” 

Där är det: fantasi—också känd som illusion, någonting som kan säljs. En kund i baren säger direkt till kameran, igen, till synes inte ironiskt: 

“Breakfast Burrito, ja—that’s Brooklyn for me.” 

Bakom honom ser man baren: dunkel, ljus på borden, kranar som erbjuder Brooklyn öl, en av de korta och tjocka Red Bull kylskåpen—Brooklyn, tydligen. 
När man tar en titt på deras utstuderade matsedel, så finns det, hur kan man säga, helt kränkande namn för deras erbjudanden: “The ‘Old’ Meat Packing [sic] District,” “The Brooklyn Beatdown,” “Black and Blue.” Vad menar dem? “Minns du (svensk person) de gamla, goda tider när The Meatpacking District var bara slakterier? Nu är det så lame.” Engelska översättning: “Den Gamla Goda Horns Tull.” 
Och vad ska man säga om “The Brooklyn Beatdown” eller “Black and Blue”—att Brooklyn är våldsfylld? Knappt två minuter före det så sa Henrik att det var “lantliga.” Ska man öppna en bar som heter “Bar Iraq” och nämna en burger “American Drone Strike” eller en slider för “Pre-Invasion Baghdad?” 

Det här är inte bara ett svenskt problem. Japaner har sina Mister Donut affärer, där det finns kartor av Brooklyn på varje bord. Paris har deras 10ème arrondissement där, i en podcast häromsistones, Benjamin Walker pratar med sin franskisk fru som djärvt påstår att en av de anledningar till att terroristerna valde det tionde distriktet var för att den hade Brooklyniserats, och “representerar framtiden av Frankrike,” gilla det eller icke. I USA med: min pojkvän berättade om hur i hans små universitetsstad fanns det en bar som hette “Manhattan Bar.” När jag frågade honom vad det hade att göra med Manhattan han sa att den likandes alla andra och visste inte. 

Det som gör fenomenet i  Sverige unikt är att, som Henrik Evrell säger i intervjun, denna “movement” började redan 2008, och nu är det 2016. Mina kompisar säger att hela fenomenet är alive and well. <Siri Isakson>, en _____ på _____, sa: 

“Kanske det bästa exemplet är en reklam för Hornstull som just nu pryder tunnelbanan där det bokstavligen står ‘Hornstull är inte Brooklyn’ så extremt reaktionärt att man spyr”

I NYC har vi nu café kedjor som heter FIKA och Konditoriet med fem lokaler  i samma förutnämnda 3 kilometers radie. Vi har svenska restauranger. Vem tycker inte om kanelbullar och lax? Men skillnaden är att de inte heter Maria Torget Restaurant, Södermalms Cafe, eller Stockholm Bar. 
Om jag skulle öppna en bar i Brooklyn som kallades för “Stockholm bar” hur skulle den se ut? Vilka idéer om Stockholm skulle dominera? Gamla Stanlig? Östermalm på en Fredagskväll? Kanske Bagarmossen Bar med en mysig inredningsdesign, träväggar, fågelkvitter, osv.? 
Eller kanske Hjulsta Bar? Man hånskrattar. Men under första hälften av min tid i Sverige var det där jag bodde, där jag såg en bil brinna upp på parkeringen tvärs över perrongen till min lägenhet, där någon knivhöggs utanför entrén, där jag såg just hur förfuskade Sveriges planering och strategier kring integration av  invandrare var (skicka dem till ett ghetto och glöm bort dem), där inga av våra vänner kom och hälsade på för att det var “för långt bort.” Meningen var ändå klar. Det var många som växte upp i Stockholm som hade aldrig varit där. 
Det är här en ännu mer myopisk och rasistisk sida av “the movement” avslöjar sig. Man ställer sig således frågan, “Varför skulle någon kalla en bar Bar Tensta, eller Bar Rinkeby, eller Bar Hjulsta om man ville att det skulle tala för Stockholm? Det är inte Stockholm.” Men jo: det är det. Jo: det talar för Stockholm. Så minns man “The Brooklyn Beatdown”?

Barer som Bar Brooklyn finns i Brooklyn! Men de finns bara i en del av Brooklyn som består av kanske 1/1,000 av hela stadsdelen. Alla ligger i gentrifierade stadsdelar, alla är smaklösa, har ingen längre  historia eller anknytning till kvarteren de ligger i, och tillgodoser en viss kundkrets—ung och vit, nämligen. Om man av någon anledning skulle orka fråga någonsom föddes i Brooklyn om de tänkte att sådana representerar Brooklyn skulle de bara fortsätta på sin väg. För att en stad, vilken stad som helst, eller ett grannskap som består av nästan tvåmiljoner individer och har en av de rikaste historier i världen, är okvantifierbar. Och det är där kruxet ligger när man säger att någonting är “Brooklyn.” Vilket Brooklyn? 

Kulturkritik som inte erbjuder några lösningar är inte kulturkritik alls—det är kulturklagomål. 
Så klart är det tillåtet – och bör uppmuntras – att respektera och beundra andra kulturer och platser. Emellertid bör folk ifrågasätta vilka kulturer som firas och till vilken grad. Varför finns det ett så knappt intresse för irakisk, eller somalisk, eller syrisk kultur i Stockholm? Efter någonting har varit i den nationella conversation kommande snart för ett decennium, då måste det åtgärdas. Kulturella processer skall ofta åtgärdas, vilket jag vet händer i Sverige, kanske även mer än i USA—man måste bara ta en överblick på TV och se hur vår politiska situation har spårats ur. 
Man ska fira sin egen kultur! Sverige och Stockholm har ju rika kulturer; jag upptäckte att jag har integrerat vissa kvaliteter och subtiliteter från min korta vistelse där. Samtidigt påstårt jag absolut inte att nationalism är lösningen/ någonsin rekommenderad. 
Men om ett folk ska integrera eller fira en annan kultur, då måste folket uppmärksamma att subtiliteter finns i alla kulturer—världen runt—och att man ska har dem i åtanke när man gör så. Att kalla någonting för “Brooklyn,” i de flesta fall, är att vara okunnig om kulturen. Brooklynisering är det nya McDondaldsisering, och i Sverige pratar man gärna om hur farlig och äcklig reduktionism kan vara. 
Skiftet till en monokultur, specifikt en västerländsk monokultur, är det som ska undvikas och det som kan förhindras. Det är möjligt att man kan fira mångfald utan att förminska det. En bra startpunkt är upphörandet att beskrivae saker som Brooklyn. 

