---
layout: post
title: Making This Site
tags: [programming, jekyll, static-site, netlify, gitlab-pages]
---

tldr: gitlab pages; amazon route 53 change DNS servers (wait at least 24 hours! mine took ~1 hour); netlify; jekyll with jekyll-new theme;
