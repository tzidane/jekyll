---
layout: post
title: Louis Aragon - Le front rouge
tags: [writing, Swedish, not-programming]
---

Une douceur pour mon chien

Un doigt de champagne Bien Madame

Nous sommes chez Maxim's l'an mil

neuf cent trente

On met des tapis sous les bouteilles

pour que leur cul d'aristocrate

ne se heurte pas aux difficultés de la vie

[...]

Il y a des fume-cigarettes entre la cigarette et l'homme

[...]

Les journées sont de feutre

les hommes de brouillard Monde ouaté

sans heurt

[...]

Et puis les bonnes œuvres font traîner des robes noires

dans des escaliers je ne vous dis que ça

La princesse est vraiment trop bonne

Pour la reconnaissance qu'on vous en a

À peine s'ils vous remercient

C'est l'exemple des bolchéviques

Malheureuse Russie

L'U.R.S.S.

L'U.R.S.S. ou comme ils disent S.S.S.R.

S.S. comment est-ce S.S.S.

S.S.R. S.S.R. S.S.S.R. oh ma chère

Pensez donc S.S.S.R.

Vous avez vu

les grèves du nord

Je connais Berck et Paris-Plage

Mais non les grèves S.S.S.R.

S.S.S.R. S.S.S.R. S.S.S.R.

[...]

Paris il n'y a pas si longtemps

que tu as vu le cortège fait à Jaurès

et le torrent Sacco-Vanzetti *

Paris tes carrefours frémissent encore de toutes leurs narines

Tes pavés sont toujours prêts à jaillir en l'air

Tes arbres à barrer la route aux soldats

[...]

Pliez les réverbères comme des fétus de paille

Faites valser les kiosques les bancs les fontaines Wallace

Descendez les flics

Camarades

Descendez les flics

Plus loin plus loin vers l’ouest où dorment

les enfants riches et les putains de première classe

Dépasse la Madelaine Prolétariat

Que ta fureur balaye l’Élysée

Tu as bien droit au Bois de Boulogne en semaine

Un jour tu feras sauter l’Arc de triomphe

Prolétariat connais ta force

Connais ta force et déchaîne-la

Il prépare son jour

il attend son heure

sa minute la seconde

où le coup porté sera mortel et la balle à ce point sûre

que tous les médecins socialfascistes

Penchés sur le corps de la victime

Auront beau promener leurs doigts chercheurs sous la chemise de dentelle

ausculter avec les appareils de précision son cœur déjà pourrissant

ils ne trouveront pas le remède habituel

et tomberont aux mains des émeutiers qui les colleront au mut

Feu sur Léon Blum

Feu sur Boncour Frossard Déat**

Feu sur les ours savants de la social-démocratie

Feu feu j’entends passer

la mort qui se jette sur Garchery**

Feu vous dis-je

Sous la conduite du parti communiste

SFIC

Vous attendez le feu sous la gâchette

Que ce ne soit plus moi qui vous crie

Feu

Mais Lénine

Le Lénine du juste moment

[…]

J’assiste à l’écrasement d’un monde hors d’usage

J’assiste avec enivrement au pilonnage des bourgeois

[•••]

Je chante la domination violente du Prolétariat sur la bourgeoisie

pour l’anéantissement de cette bourgeoisie

pour l’anéantissement total de cette bourgeoisie

[...]

L'éclat des fusillades ajoute au paysage

une gaîté jusqu'alors inconnue

Ce sont des ingénieurs des médecins qu'on exécute

Mort à ceux qui mettent en danger les conquêtes d'Octobre

Mort aux saboteurs du Plan quinquennal

[...]

À vous Jeunesses communistes

[...]

Dressez-vous contre vos mères

Abandonnez la nuit la peste et la famille

Vous tenez dans vos mains un enfant rieur

un enfant comme on n'en a jamais vu

Il sait avant de parler toutes les chansons de la nouvelle vie

Il va vous échapper Il court Il rit déjà

Les astres descendent familièrement sur la terre

C'est bien le moins qu'ils brûlent en se posant

la charogne noir des égoïstes

[...]

Les yeux bleus de la Révolution

brillent d'une cruauté nécessaire

SSSR SSSR SSSR

[...]

Voici la catastrophe apprivoisée

voici docile enfin la bondissante panthère

l'Histoire menée en laisse par la Troisième Internationale

Le train route s'ébranle et rien ne l'arrêtera

[...]

C'est le chant de l'homme et son rire

C'est le train de l'étoile rouge

qui brûle les gares les signaux les airs

SSSR Octobre octobre c'est l'express

Octobre à travers l'univers SS

SR SSSR SSSR SSSR SSSR

