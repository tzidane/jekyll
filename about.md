---
layout: post
tags: [about, programming, cv]
---

## Developer | [CV]({{ site.url }}/cv) | Writer


<a href="https://github.com/{{ site.footer-links.github }}"><i class="svg-icon github"></i></a>

### My Projects 

* <a href="http://magallery.herokuapp.com" target="_blank">#MAGAllery</a>
  * Node application using Socket.io and Twit.js to parse responses from
Twitter's API of users tagging #MAGA

* <a href="https://github.com/saylestyler/spinodeza" target="_blank">SpiNODEza</a>
  * CLI Spinoza-quote fetcher

* <a href="https://github.com/saylestyler/iexport" target="_blank">iExport</a>
  * Python plaything for rapid backup of iMessage local databases

* <a href="#">Wamo</a>
  * WIP! Native iOS app mapping of <a  href="http://www1.nyc.gov/nyc-resources/service/2479/minority-and-woman-owned-business-enterprise-mwbe-program" target="_none">Minority & Women Owned Businesses  </a> in NYC

<hr/>
## Fave Stuff

#### github stuff
<ul>
{% for gh in site.data.ghdata %}
  <li>
    <a href="https://www.github.com/{{ gh.full_name }}">
    {{ gh.full_name }}
    </a>
  </li>
{% endfor %}
</ul>

#### fave mix stuff:
<iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/332116572&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>

