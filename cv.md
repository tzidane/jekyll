---
layout: post
tags: about, programming, cv
---

<div style="text-align: center">
<strong>Tyler Sayles</strong>
<br>
saylestyler@gmail.com | <a href="http://www.tylsyl.com">tylsyl.com</a>

<br>

<a href="">github</a> • <a href="">stackoverflow</a> • <a href="">linkedin</a>

</div>
## Professional

<hr>

### VFILES | Full Stack + iOS Developer

2015—Present

* Built new product features, maintained and optimized for both an iOS
app and site
* Integrated a Wordpress-based news platform into site and app
including: provisioning new AWS
instances, VPNs, etc., to writing the front-end, to teaching clients
how they could use it to publish
their own articles
* Integrated Livestream.com with JW Player for livestreaming on site
and app
* Creating a variety of Haskell micro-services
* Held company-wide meetings for product feedback
* Acted as point-person for all communication with dev team
* Deployed the site often and with zero downtime
* Released new versions of the app to the App Store after QA via
TestFlight


## Personal

<hr>

### [Wamo](tylsyl.com/#)

* Native iOS app mapping NYC’s Minority and Women-Owned Business Program (W/MBE)

### [MAGAllery](http://magallery.herokuapp.com)

* Node application using Socket.io and Twit.js to parse responses from
Twitter's API of users tagging #MAGA

### [SpiNODEza](http://github.com/saylestyler/spinodeza)

* CLI Spinoza-quote fetcher

### [iExport](http://github.com/saylestyler/iexport)

* Python plaything for rapid backup of iMessage local databases


## Skills / Languages

<hr>

* JavaScript (Vanilla, TS, Coffee, ES6, AngularJS, Node),
Swift, PHP, SQL, CSS (and its precompilers), Shell (personal favorite), Haskell

* AWS (EC2, EB, ELB, RDS, S3), iOS, Postgres, Wordpress, Heroku,
Magento, CircleCI, Nginx, Jekyll, Unix
